# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import calendar
import logging
import pytz

from odoo import api, fields, models, _
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)

class IrSequence(models.Model):
    _inherit = 'ir.sequence'

    use_monthly_date_range = fields.Boolean(string='Use monthly subsequences per date_range')
    use_daily_date_range = fields.Boolean(string='Use daily subsequences per date_range')

    def _get_prefix_suffix(self, date=None, date_range=None):
        def int_to_Roman(num):
            lookup = [
                (1000, 'M'),
                (900, 'CM'),
                (500, 'D'),
                (400, 'CD'),
                (100, 'C'),
                (90, 'XC'),
                (50, 'L'),
                (40, 'XL'),
                (10, 'X'),
                (9, 'IX'),
                (5, 'V'),
                (4, 'IV'),
                (1, 'I'),
            ]
            res = ''
            for (n, roman) in lookup:
                (d, num) = divmod(num, n)
                res += roman * d
            return res

        def _interpolate(s, d):
            return (s % d) if s else ''

        def _interpolation_dict():
            now = range_date = effective_date = datetime.now(pytz.timezone(self._context.get('tz') or 'UTC'))
            if date or self._context.get('ir_sequence_date'):
                effective_date = fields.Datetime.from_string(date or self._context.get('ir_sequence_date'))
            if date_range or self._context.get('ir_sequence_date_range'):
                range_date = fields.Datetime.from_string(date_range or self._context.get('ir_sequence_date_range'))

            sequences = {
                'year': '%Y', 'month': '%m', 'day': '%d', 'y': '%y', 'doy': '%j', 'woy': '%W',
                'weekday': '%w', 'h24': '%H', 'h12': '%I', 'min': '%M', 'sec': '%S',
            }
            res = {}
            for key, format in sequences.items():
                res[key] = effective_date.strftime(format)
                res['roman_' + key] = int_to_Roman(int(effective_date.strftime(format)))
                res['range_' + key] = range_date.strftime(format)
                res['current_' + key] = now.strftime(format)

            return res

        self.ensure_one()
        d = _interpolation_dict()
        try:
            interpolated_prefix = _interpolate(self.prefix, d)
            interpolated_suffix = _interpolate(self.suffix, d)
        except ValueError:
            raise UserError(_('Invalid prefix or suffix for sequence \'%s\'') % self.name)
        return interpolated_prefix, interpolated_suffix

    @api.onchange('use_date_range')
    def _onchange_use_date_range(self):
        if not self.use_date_range:
            self.use_monthly_date_range = False
            self.use_daily_date_range = False

    @api.onchange('use_monthly_date_range', 'use_daily_date_range')
    def _onchange_use_daily_monthly_date_range(self):
        if self.use_monthly_date_range or self.use_daily_date_range:
            self.use_date_range = True

    def _next(self, sequence_date=None):
        """ Returns the next number in the preferred sequence in all the ones given in self."""
        if not self.use_date_range:
            return self._next_do()
        # date mode
        dt = sequence_date or self._context.get('ir_sequence_date', fields.Date.today())
        seq_date = self.env['ir.sequence.date_range'].search([('sequence_id', '=', self.id), ('date_from', '<=', dt), ('date_to', '>=', dt)], limit=1)
        if not seq_date:
            if self.use_daily_date_range:
                seq_date = self._create_daily_date_range_seq(dt)
            elif self.use_monthly_date_range:
                seq_date = self._create_monthly_date_range_seq(dt)
            else:
                seq_date = self._create_date_range_seq(dt)
        return seq_date.with_context(ir_sequence_date_range=seq_date.date_from)._next()

    def _create_daily_date_range_seq(self, date):
        date = fields.Date.from_string(date).strftime('%Y-%m-%d')
        date_from = date
        date_to = date
        seq_date_range = self.env['ir.sequence.date_range'].sudo().create({
            'date_from': date_from,
            'date_to': date_to,
            'sequence_id': self.id,
        })
        return seq_date_range
    
    def _create_monthly_date_range_seq(self, date):
        year = fields.Date.from_string(date).strftime('%Y')
        month = fields.Date.from_string(date).strftime('%m')
        date_from = '{}-{}-01'.format(year, month)
        date_to = '{}-{}-{}'.format(year, month, calendar.monthrange(int(year), int(month))[1])
        seq_date_range = self.env['ir.sequence.date_range'].sudo().create({
            'date_from': date_from,
            'date_to': date_to,
            'sequence_id': self.id,
        })
        return seq_date_range

    @api.model
    def _get_current_sequence(self, sequence_date=None):
        '''Returns the object on which we can find the number_next to consider for the sequence.
        It could be an ir.sequence or an ir.sequence.date_range depending if use_date_range is checked
        or not. This function will also create the ir.sequence.date_range if none exists yet for today
        '''
        if not self.use_date_range:
            return self
        sequence_date = sequence_date or fields.Date.today()
        seq_date = self.env['ir.sequence.date_range'].search(
            [('sequence_id', '=', self.id), ('date_from', '<=', sequence_date), ('date_to', '>=', sequence_date)], limit=1)
        if seq_date:
            return seq_date[0]
        #no date_range sequence was found, we create a new one
        if self.use_daily_date_range:
            return self._create_daily_date_range_seq(sequence_date)
        elif self.use_monthly_date_range:
            return self._create_monthly_date_range_seq(sequence_date)
        return self._create_date_range_seq(sequence_date)