# -*- coding: utf-8 -*-
{
    'name': 'Sequence Daily',
    'version': '0.1.0',
    'category': 'Base',
    'author': 'DNAomissions',
    'depends': ['base'],
    'data': ['views/ir_sequence_views.xml'],
    'demo': [],
    'installable': True,
    'auto_install': False,
}
